<?php


namespace App\Http\Controllers;


use App\Http\Requests\ProductManageRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index()
    {
        return ProductResource::collection(Product::all());
    }

    public function store(ProductManageRequest $request)
    {
        $product = Product::create($request->all());
        $product->categories()->sync($request->categories);
        return new ProductResource($product);
    }

    public function update(ProductManageRequest $request, Product $product)
    {
        $product->fill($request->all())->save();
        $product->categories()->sync($request->categories);
        return new ProductResource($product);
    }

    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    public function destroy(Product $product)
    {
        try {
            return response(204)->json($product->delete());
        } catch (\Exception $e) {
            return response(400);
        }
    }
}
