<?php


namespace App\Http\Controllers;


use App\Http\Requests\CategoryManageRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    public function store(CategoryManageRequest $request)
    {
        return new CategoryResource(Category::create($request->all()));
    }

    public function update(CategoryManageRequest $request, Category $category)
    {
        $category->fill($request->all())->save();
        return new CategoryResource($category);
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function destroy(Category $category)
    {
        if($category->products) {
            return response(400, 'Category not empty!');
        }
        try {
            return response(204)->json($category->delete());
        } catch (\Exception $e) {
            return response(400);
        }
    }
}
